<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
	<title>Product List Page</title>
</head>
<body>
	<h1 align="center" style="color: blue; font-family: inherit;">List of Products</h1>
	<div align="center">
		<form action="/products/currency" method="post">
			<b style="color: green;">Choose Currency:</b> 
			<select name="currency" onchange='this.form.submit()'>
				<c:forEach items="${currencyList}" var="currencyValue">
					<c:choose>
						<c:when test="${currencyValue eq currency}">
							<option value="${currencyValue}" selected="true">${currencyValue}</option>
						</c:when>
						<c:otherwise>
							<option value='${currencyValue}'>${currencyValue}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
		</form>
	</div><br>
	<table align="center" border="1">
		<tr>
			<th>Product Name  </th>
			<th>Price         </th>
			<th>Edit Product  </th>
		</tr>
		<c:forEach items="${productsList}" var="product">
			<tr>
				<td><c:out value="${product.name}"></c:out></td>
				<td>${product.prices[currency].amount}</td>
				<td>
					<form action="/product/edit" method="post" id="editform${product.id}">
						<input type="hidden" name="productId" value="${product.id}" id="productId">
						<a onclick="document.forms['editform${product.id}'].submit();"><button>Edit</button></a>
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>