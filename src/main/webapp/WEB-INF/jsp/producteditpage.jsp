<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<html lang="en">
	<head>
		<title>Product Edit Page</title>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script src="<c:url value="/js/currencySelector.js" />"></script>
	</head>
	<body align="center">
		<h1>Product Edit Page</h1>
		<h3 style="color: green">${message}</h3>
		<form:form modelAttribute ="productForm" action="/product/update" method="post" id="editform">
			<form:input id="id" path="id" hidden="hidden" />
			<table align="center" border="1">
				<tr>
					<th>Product Name</th>
					<td><form:input path="name" /> <form:errors path="name" cssClass="error" /></td>
				</tr>
				<tr>
					<th>Description</th>
					<td><form:input path="description" /> <form:errors path="description" cssClass="error" /></td>
				</tr>
				<tr>
					<th>Currency</th>
					<td><form:select path="currency" id="currency">
							<form:options items="${currencyList}" />
						</form:select>
					<form:errors path="currency" cssClass="error" /></td>
				</tr>
				<tr>
					<th>Amount</th>
					<td><form:input path="price" type="number" step="0.01" min="1"
							id="price" /> <form:errors path="price" cssClass="error" /></td>
				</tr>
			</table>
			<br>
			<button class="button"><a href="/products/list" style="width: 164px;"/>Back</button>
			<a onclick="document.forms['editform'].submit();"><button class="button">SAVE</button></a>
		</form:form>
	</body>
</html>
