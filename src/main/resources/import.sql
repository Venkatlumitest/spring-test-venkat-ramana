insert into product(name, description) values ('Product 1', 'An awesome product 1');
insert into product(name, description) values ('Product 2', 'An awesome product 1');

insert into price(product_id, currency, amount) values (1, 'GBP', 9.99);
insert into price(product_id, currency, amount) values (1, 'EUR', 11.99);
insert into price(product_id, currency, amount) values (2, 'GBP', 19.99);
insert into price(product_id, currency, amount) values (2, 'EUR', 23.50);

insert into product(id, name, description) values (3, 'Product 3', 'An awesome product 3');
insert into product(id, name, description) values (4, 'Product 4', 'Product 4 is the best');

insert into price(id, product_id, currency, amount) values (5, 3, 'GBP', 10.99);
insert into price(id, product_id, currency, amount) values (6, 3, 'EUR', 12.99);
insert into price(id, product_id, currency, amount) values (7, 4, 'GBP', 13.99);
insert into price(id, product_id, currency, amount) values (8, 4, 'EUR', 24.50);
