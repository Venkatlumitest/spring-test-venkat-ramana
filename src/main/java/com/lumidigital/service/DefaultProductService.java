package com.lumidigital.service;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.lumidigital.forms.ProductForm;
import com.lumidigital.model.Price;
import com.lumidigital.model.Product;

@Component("productService")
@Transactional
public class DefaultProductService implements ProductService {

	private final ProductRepository productRepository;

	public DefaultProductService(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Override
	public Iterable<Product> getProducts() {
		return productRepository.findAll();
	}

	@Override
	public Product getProductForId(Long id) {
		Assert.notNull(id, "Id must not be null");
		return productRepository.findById(id);
	}

	@Override
	public Product save(Product product) {
		return productRepository.save(product);
	}
	
	@Override
	public Product updateProduct(ProductForm productForm) {
		Product product = getProductForId(Long.valueOf(productForm.getId()));
		if (null != product)
		{
			product.setName(productForm.getName());
			product.setDescription(productForm.getDescription());
			product.setPrices(updatedProductPrices(product.getPrices(), productForm));
			save(product);
			return product;
		}
		return null;
	}
	private Map<Currency, Price> updatedProductPrices(Map<Currency, Price> prices, ProductForm productForm)
	{
		for (Entry<Currency, Price> entry : prices.entrySet())
		{
			Currency currency = entry.getKey();
			Price price = entry.getValue();
			if(productForm.getCurrency().equals(currency.getCurrencyCode()))
			{
				price.setAmount(new BigDecimal(productForm.getPrice()));
				prices.put(currency, price);
				break;
			}
		}
		return prices;
	}

}
