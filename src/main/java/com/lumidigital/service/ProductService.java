package com.lumidigital.service;

import com.lumidigital.forms.ProductForm;
import com.lumidigital.model.Product;

public interface ProductService {

	Iterable<Product> getProducts();
	Product getProductForId(Long id);
	Product save(Product product);
	Product updateProduct(ProductForm productForm);
}
