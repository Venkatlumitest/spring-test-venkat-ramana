package com.lumidigital.service;

import javax.servlet.http.HttpServletRequest;

public interface SessionService {
	void setSessionCurrency(HttpServletRequest request, String currency);
}
