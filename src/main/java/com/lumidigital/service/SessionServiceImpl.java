package com.lumidigital.service;

import java.util.Currency;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;


@Component("sessionService")
public class SessionServiceImpl implements SessionService{

private static final String CURRENCY = "currency";
	
	@Value("${default.currency}")
	private String defaultCurrency;
	@Override
	public void setSessionCurrency(HttpServletRequest request, String currency) {
		
		
		// Get the session from http request
		HttpSession session = request.getSession();

		if(StringUtils.isEmpty(session.getAttribute(CURRENCY)) && Objects.isNull(currency)) 
		{
		session.setAttribute(CURRENCY, Currency.getInstance(defaultCurrency));
		} 
		else if (!StringUtils.isEmpty(session.getAttribute(CURRENCY)) && Objects.nonNull(currency)) 
		{
		session.setAttribute(CURRENCY, Currency.getInstance(currency));
		}
		
	}
}
