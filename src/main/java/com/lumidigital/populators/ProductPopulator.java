package com.lumidigital.populators;

import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import com.lumidigital.data.PriceData;
import com.lumidigital.data.ProductData;
import com.lumidigital.forms.ProductForm;
import com.lumidigital.model.Price;
import com.lumidigital.model.Product;

@Component("productPopulator")
public class ProductPopulator {
	public List<ProductData> populate(Iterable<Product> products)
	{
		List<ProductData> productsData = new ArrayList<>();
		if (null != products && products.iterator().hasNext())
		{
			for (Product product : products)
			{
				productsData.add(populate(product));
			}
		}
		return productsData;
	}
	
	public ProductData populate(Product product)
	{
		if (null != product)
		{
			ProductData productData = new ProductData();
			productData.setName(product.getName());
			productData.setId(product.getId());
			productData.setName(product.getName());
			productData.setDescription(product.getDescription());
			productData.setPrices(populatePrice(product.getPrices()));
			return productData;
		}
		return null;
	}

	public ProductForm populate(final ProductData productData, final ProductForm productForm, final String currency)
	{
		productForm.setId(productData.getId().toString());
		productForm.setDescription(productData.getDescription());
		productForm.setName(productData.getName());
		productForm.setCurrency(currency);
		for (Entry<Currency, PriceData> price : productData.getPrices().entrySet())
		{
			if(price.getKey().getCurrencyCode().equals(currency))
			{
				productForm.setPrice(price.getValue().getAmount().toString());
				break;
			}
		}
		return productForm;
	}
	private Map<Currency, PriceData> populatePrice(Map<Currency, Price> prices) 
	{
		if (null != prices && !prices.isEmpty())
		{
			Map<Currency, PriceData> pricesData = new HashMap<>();
			for (Entry<Currency, Price> price : prices.entrySet())
			{
				PriceData priceData = new PriceData();
				priceData.setAmount(price.getValue().getAmount());
				pricesData.put(price.getKey(), priceData);
			}
			return pricesData;
		}
		return null;
	}

}
