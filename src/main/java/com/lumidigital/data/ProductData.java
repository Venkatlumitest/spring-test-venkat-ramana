package com.lumidigital.data;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;


public class ProductData {
	private Long id; 
	private String name;
	private String description;
	private Map<Currency, PriceData> prices = new HashMap<>();
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Map<Currency, PriceData> getPrices() {
		return prices;
	}
	public void setPrices(Map<Currency, PriceData> prices) {
		this.prices = prices;
	}
	
	
}
