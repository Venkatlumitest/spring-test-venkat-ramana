package com.lumidigital.data;

import java.math.BigDecimal;

public class PriceData {
	private BigDecimal amount;

	public BigDecimal getAmount()
	{
		return amount;
	}

	public void setAmount(BigDecimal amount)
	{
		this.amount = amount;
	}

}
