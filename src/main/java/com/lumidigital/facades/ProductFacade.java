package com.lumidigital.facades;

import java.util.List;

import javax.validation.Valid;

import com.lumidigital.data.ProductData;
import com.lumidigital.forms.ProductForm;

public interface ProductFacade {
	List<ProductData> getAllProduct();

	ProductData productById(Long productId);

	ProductData updateProduct(@Valid ProductForm productForm);
}
