package com.lumidigital.facades;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lumidigital.data.ProductData;
import com.lumidigital.forms.ProductForm;
import com.lumidigital.model.Product;
import com.lumidigital.populators.ProductPopulator;
import com.lumidigital.service.ProductService;

@Component("productFacade")
public class ProductFacadeImpl implements ProductFacade {

	@Autowired
	private ProductPopulator productPopulator;

	@Autowired
	private ProductService productService;

	@Override
	public List<ProductData> getAllProduct() {

		Iterable<Product> products = productService.getProducts();
		return productPopulator.populate(products);
	}

	@Override
	public ProductData productById(Long productId) {

		return productPopulator.populate(productService.getProductForId(productId));
	}

	
	  @Override 
	  public ProductData updateProduct(ProductForm productForm) { Product
	  updatedProduct = productService.updateProduct(productForm);
	  return productPopulator.populate(updatedProduct); }
	 
}
