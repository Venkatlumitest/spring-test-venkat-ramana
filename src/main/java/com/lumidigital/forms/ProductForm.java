package com.lumidigital.forms;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class ProductForm {
	private String id;
	private String description;
	@NotBlank(message="{product.name.empty}")
	@Size(max=50, message = "{product.name.maxlength}")
	private String name;
	
	@NotBlank(message = "{product.price.empty}")
	@DecimalMin("1.00")
	private String price;
	
	private String currency;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	
}
