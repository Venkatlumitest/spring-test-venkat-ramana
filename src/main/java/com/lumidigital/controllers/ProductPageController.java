package com.lumidigital.controllers;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lumidigital.data.ProductData;
import com.lumidigital.facades.ProductFacade;
import com.lumidigital.service.SessionService;


@Controller
@RequestMapping("/products")
public class ProductPageController {
	private static final String PRODUCT_LIST_PAGE = "productlistpage";
	private static final String CURRENCY = "currency";
	private static final String REDIRECT_TO_PRODUCT_LIST_PAGE = "redirect:/products/list";
	
	@Autowired
	private ProductFacade productFacade;
	
	@Resource
	private SessionService sessionService;
	
	@RequestMapping("/list")
	public String getProductListPage(final Model model,final HttpServletRequest request)
	{
		model.addAttribute("productsList", productFacade.getAllProduct());
		if(StringUtils.isEmpty(request.getAttribute(CURRENCY)))
			sessionService.setSessionCurrency(request, null);	
		
		return PRODUCT_LIST_PAGE;
	}
	
	
	@Value("#{'${supported.currency.codes}'.split(',')}")
    private List<String> supportedCurrencies;

    @ModelAttribute("currencyList")
    public List<String> getSupportedCurrencies() 
    {
        return supportedCurrencies;
    }
    
    
    @RequestMapping(value = "/currency", method = { RequestMethod.GET, RequestMethod.POST })
	public String getProductsByCurrency(final HttpServletRequest request, final Model model, @RequestParam(name = CURRENCY) final String currency)
	{
		sessionService.setSessionCurrency(request, currency);	
		return REDIRECT_TO_PRODUCT_LIST_PAGE;
	}
    
    
    @ResponseBody
	@RequestMapping(value = "/jsonData", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ProductData> getAllProductDataJson() 
	{
		return productFacade.getAllProduct();
	}
}
