package com.lumidigital.controllers;

import java.util.Currency;
import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.lumidigital.data.ProductData;
import com.lumidigital.facades.ProductFacade;
import com.lumidigital.forms.ProductForm;
import com.lumidigital.populators.ProductPopulator;


@Controller
@RequestMapping("/product")
public class ProductEditPageController {

	private static final String PRODUCT_EDIT_PAGE = "producteditpage";
	private static final String CURRENCY = "currency";
	
	@Autowired
	private ProductFacade productFacade;
	@Autowired
	private ProductPopulator productPopulator;
	
	
	@RequestMapping(value = "/edit",method = RequestMethod.POST)
	public String editProduct(final Model model,final HttpServletRequest request, @RequestParam(value = "productId", required = false) final String productId,final ProductForm productForm) 
	{
		HttpSession session = request.getSession();
		Currency currency = (Currency) session.getAttribute(CURRENCY);
		ProductData productData = productFacade.productById(Long.parseLong(productId));
		ProductForm form = productPopulator.populate(productData, productForm, currency.getCurrencyCode());
		model.addAttribute("productForm", form);
		return PRODUCT_EDIT_PAGE;
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateProduct(final HttpServletRequest request, @Valid ProductForm productForm, final BindingResult bindingResult,final Model model)
	{
		if(!bindingResult.hasErrors())
		{
			ProductData productData = productFacade.updateProduct(productForm);
			if (Objects.nonNull(productData))
			{
                model.addAttribute("message", "Product updated Succesfully");
			}
		}
		return PRODUCT_EDIT_PAGE;
	}

	
	@Value("#{'${supported.currency.codes}'.split(',')}")
    private List<String> supportedCurrencies;

    @ModelAttribute("currencyList")
    public List<String> getSupportedCurrencies() 
    {
        return supportedCurrencies;
    }
	
}
